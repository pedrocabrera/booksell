-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Lun 16 Décembre 2019 à 19:35
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `booksell`
--

-- --------------------------------------------------------

--
-- Structure de la table `annonce`
--

CREATE TABLE `annonce` (
  `ID` varchar(63) NOT NULL,
  `ETAT` varchar(50) NOT NULL,
  `PRIX` double NOT NULL,
  `ID_LIVRE` int(13) NOT NULL,
  `ID_UTILISATEUR` int(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `annonce`
--

INSERT INTO `annonce` (`ID`, `ETAT`, `PRIX`, `ID_LIVRE`, `ID_UTILISATEUR`) VALUES
('ef3fc5c1-29c3-49b2-8506-3e1a119b662a', 'acceptable', 40, 4, 9);

-- --------------------------------------------------------

--
-- Structure de la table `livres`
--

CREATE TABLE `livres` (
  `ID` int(13) NOT NULL,
  `TITRE` varchar(70) DEFAULT NULL,
  `AUTEUR` varchar(80) DEFAULT NULL,
  `EDITEUR` varchar(70) DEFAULT NULL,
  `ANNEE` int(4) NOT NULL,
  `DESCRIPTION` varchar(250) NOT NULL,
  `CATEGORIE` varchar(20) NOT NULL,
  `IMAGE` varchar(255) DEFAULT NULL,
  `VALIDE` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `livres`
--

INSERT INTO `livres` (`ID`, `TITRE`, `AUTEUR`, `EDITEUR`, `ANNEE`, `DESCRIPTION`, `CATEGORIE`, `IMAGE`, `VALIDE`) VALUES
(1, '1984 - Folio', 'Orwell, George', 'Folio', 2014, '', 'Francais', 'http://www.cooprosemont.qc.ca/ib/000023840000/CO_IMAGESBANK_MAST1/9782/0703/6822/8/9782070368228/medium_9782070368228.jpg', 0),
(2, 'À toi pour toujours, ta Marie-Lou', 'Tremblay, Michel', 'Actes Sud', 2007, '', 'Francais', 'http://www.cooprosemont.qc.ca/ib/000023840000/CO_IMAGESBANK_MAST1/9782/7427/7124/0/9782742771240/medium_9782742771240.jpg', 0),
(3, 'Adversaire (L\') - Folio', 'Carrière, Emmanuel', 'Gallimard', 2001, '', 'Francais', 'http://www.cooprosemont.qc.ca/ib/000023840000/CO_IMAGESBANK_MAST1/9782/0704/1621/9/9782070416219/medium_9782070416219.jpg', 1),
(4, 'Albertine, en cinq temps', 'Tremblay, Michel', 'Actes Sud', 2007, '', 'Francais', 'http://www.cooprosemont.qc.ca/ib/000023840000/CO_IMAGESBANK_MAST1/9782/7427/7123/3/9782742771233/medium_9782742771233.jpg', 1),
(5, 'Amélioration du français écrit', 'Beaudin, Karoline', 'CHENELIÈRE ÉDUCATION', 2016, '', 'Francais', 'http://www.cooprosemont.qc.ca/ib/000023840000/CO_IMAGESBANK_MAST1/9782/7650/4966/1/9782765049661/medium_9782765049661.jpg', 1),
(6, 'Anthologie de la littérature : Du Moyen âge à 1850', 'Chénier, Jean-François', 'ERPI', 2015, '', 'Francais', 'http://www.cooprosemont.qc.ca/ib/000023840000/CO_IMAGESBANK_MAST1/9782/7613/5510/0/9782761355100/medium_9782761355100.jpg', 1),
(7, 'Anthologie de la littérature fantastique (Grands textes)', 'Limoges, Alexandre', 'CEC', 2015, '', 'Francais', 'http://www.cooprosemont.qc.ca/ib/000023840000/CO_IMAGESBANK_MAST1/9782/7617/7861/9/9782761778619/medium_9782761778619.jpg', 1),
(8, 'Anthologie de la littérature québécoise, 3e édition', 'Vaillancourt, Claude', 'Beauchemin', 2017, '', 'Francais', 'http://www.cooprosemont.qc.ca/ib/000023840000/CO_IMAGESBANK_MAST1/9782/7616/6209/3/9782761662093/medium_9782761662093.jpg', 1),
(9, 'Anthologie litt. du Moyen Âge au XIXe siècle (3 éd.)', 'Laurin, Michel', 'Beauchemin', 2012, '', 'Francais', 'http://www.cooprosemont.qc.ca/ib/000023840000/CO_IMAGESBANK_MAST1/9782/7616/5742/6/9782761657426/medium_9782761657426.jpg', 1),
(10, 'Chimie organique 1', 'Girouard Stéphane,Lapierre Danielle,Marrano Claudio', 'Chenelière', 2013, '', 'Chimie', 'http://www.cooprosemont.qc.ca/ib/000023840000/CO_IMAGESBANK_MAST1/9782/7650/3356/1/9782765033561/medium_9782765033561.jpg', 1),
(11, 'Chimie des solutions, Une approche moléculaire, 2e édition', 'Champagne, Marielle', 'ERPI', 2016, '', 'Chimie', 'https://images-na.ssl-images-amazon.com/images/I/41StQ2L13PL._SX383_BO1,204,203,200_.jpg', 1),
(12, 'Chimie générale- Une approche moléculaire, 2e édition', 'Tro, Nivaldo J.', 'Pearson Erpi', 2018, '', 'Chimie', 'https://www.coopuqam.com/DATA/ITEM/grande/585373~v~Chimie_generale___une_approche_moleculaire.jpg', 1),
(13, 'Calcul différentiel, 2e édition', 'Brunelle, Éric et Désautels, Marc-André', 'CEC', 2016, '', 'Mathématiques', 'http://www.cooprosemont.qc.ca/ib/000023840000/CO_IMAGESBANK_MAST1/9782/7617/8880/9/9782761788809/medium_9782761788809.jpg', 1),
(14, 'Calcul intégral, 5e édition', 'Charron, Gilles et Parent, Pierre', 'Chenelière', 2015, '', 'Mathématiques', 'http://www.cooprosemont.qc.ca/ib/000023840000/CO_IMAGESBANK_MAST1/9782/7650/4748/3/9782765047483/medium_9782765047483.jpg', 1),
(15, 'Mathématiques discrètes, édition révisée', 'Rosen, Kenneth H.', 'Chenelière', 2002, '', 'Mathématiques', 'http://www.cooprosemont.qc.ca/ib/000023840000/CO_IMAGESBANK_MAST1/9782/8946/1642/0/9782894616420/medium_9782894616420.jpg', 1),
(16, 'Mise à niveau mathématiques. 2e édition', 'hamel, Josée', 'Erpi sciences', 2017, '', 'Mathématiques', 'http://www.cooprosemont.qc.ca/ib/000023840000/CO_IMAGESBANK_MAST1/9782/7613/7560/3/9782761375603/medium_9782761375603.jpg', 1),
(17, 'Notions de statistiques, 3e édition', 'Simard, Christiane', 'Modulo', 2015, '', 'Mathématiques', 'http://www.cooprosemont.qc.ca/ib/000023840000/CO_IMAGESBANK_MAST1/9782/8973/2017/1/9782897320171/medium_9782897320171.jpg', 1),
(18, 'Modèles mathématiques informatique, 2e edition', 'Ross, André', 'Prodafor inc', 2019, '', 'Mathématiques', 'https://www.zone.coop/fileadmin/images/produits/617424_big.jpg', 1),
(19, 'Discours sur l\'origine et les fondements de l\'inégalité parm', 'Marsolais, Patricia', 'CEC', 2009, '', 'Philosophie', 'http://www.cooprosemont.qc.ca/ib/000023840000/CO_IMAGESBANK_MAST1/9782/7617/2525/5/9782761725255/medium_9782761725255.jpg', 1),
(20, 'Philosophie: Les grandes questions', 'Monnier, Anne-Emmanuelle', 'Les bagages culture', 2018, '', 'Philosophie', 'https://images-na.ssl-images-amazon.com/images/I/51RoQ8gfHjL._SX258_BO1,204,203,200_.jpg', 1),
(21, 'Les grandes questions de la philosophie de l\'Antiquité à nos jours.', 'Marie-Reine Morville', 'Delagrave', 1998, '', 'Philosophie', 'https://products-images.di-static.com/image/marie-reine-morville-les-grandes-questions-de-la-philosophie-de-l-antiquite-a-nos-jours/9782206083841-200x303-1.jpg', 1),
(22, 'La peste Albert Camus', 'Albert Camus', 'Folio', 1972, '', 'Philosophie', 'https://images-na.ssl-images-amazon.com/images/I/41ap0wZmkFL._SX302_BO1,204,203,200_.jpg', 1),
(23, 'Physique Vol.1., Mécanique', 'Benson, Harris', 'ERPI', 2015, '', 'Physique', 'http://www.cooprosemont.qc.ca/ib/000023840000/CO_IMAGESBANK_MAST1/9782/7613/7886/4/9782761378864/medium_9782761378864.jpg', 1),
(24, 'Physique XX. Vol. B, Électricité et magnétisme', 'Séguin Marc,Descheneau Julie,Tardif Benjamin', 'ERPI', 2010, '', 'Physique', 'http://www.cooprosemont.qc.ca/ib/000023840000/CO_IMAGESBANK_MAST1/9782/7613/3076/3/9782761330763/medium_9782761330763.jpg', 1),
(25, 'Physique', 'Eugene Hecht', 'Boeck', 2015, '', 'Physique', 'https://images-na.ssl-images-amazon.com/images/I/414hWhnlQ2L._SX420_BO1,204,203,200_.jpg', 1),
(26, 'Stewart - Calcul différenciel', 'James Stewart', 'Modulo', 2012, '', 'Mathématiques', 'http://www.renaud-bray.com/ImagesEditeurs/PG/1356/1356313-gf.jpg', 1),
(27, 'Physique XXI Tome A : mécanique', ' Jule Descheneau,Marc Seguin et Benjamin Tardif', 'ERPI', 2010, '', 'Physique', 'http://www.renaud-bray.com/ImagesEditeurs/PG/899/899614-gf.jpg', 1),
(28, 'Chimie générale, 4e édition ', 'Raymond Chang, Kenneth A. Goldsby, Azélie Arpin, Luc Papillon', 'CHENELIÈRE', 2014, '', 'Chimie', 'https://www.cheneliere.ca/DATA/ITEM/8781.jpg', 1),
(29, 'Candide', ' Voltaire,Stéphane Maltere,Christine Girodias-Majeune', 'MAGNARD', 2013, '', 'Francais', 'https://images-na.ssl-images-amazon.com/images/I/41fgs7rdA8L._SX359_BO1,204,203,200_.jpg', 1),
(30, 'Chimie : mise à niveau 2e édition', 'Julien, Johanne', 'MODULO', 2016, '', 'Chimie', 'https://www.cooprosemont.qc.ca/ib/000023840000/CO_IMAGESBANK_MAST1/9782/8973/2047/8/9782897320478/medium_9782897320478.jpg', 1),
(31, 'Physique Vol.2, Électricité et magnétisme', 'Benson, Harris', 'ERPI', 2015, '', 'Physique', 'https://www.cooprosemont.qc.ca/ib/000023840000/CO_IMAGESBANK_MAST1/9782/7613/7890/1/9782761378901/medium_9782761378901.jpg', 1),
(32, 'Introduction à la philosophie 4e éd.', 'Larramée, Hélène', 'Chenelière', 2013, '', 'Philosophie', 'https://www.cooprosemont.qc.ca/ib/000023840000/CO_IMAGESBANK_MAST1/9782/7650/4003/3/9782765040033/medium_9782765040033.jpg', 1),
(33, 'Grammar Guide (The) 2e cycle', 'Collectif', 'ERPI', 2012, '', 'Anglais', 'https://www.cooprosemont.qc.ca/ib/000023840000/CO_IMAGESBANK_MAST1/9782/7613/5238/3/9782761352383/medium_9782761352383.jpg', 1),
(34, 'Avenues 1: 2016 student skills', 'Lynne Gaetz', 'ERPI', 2016, '', 'Anglais', 'https://www.cooprosemont.qc.ca/ib/000023840000/CO_IMAGESBANK_MAST1/9782/7613/7789/8/9782761377898/medium_9782761377898.jpg', 1),
(35, 'Market Leader Intermediate 3E', 'Collectif', 'ERPI', 2016, '', 'Anglais', 'https://www.cooprosemont.qc.ca/ib/000023840000/CO_IMAGESBANK_MAST1/9781/4082/3695/6/9781408236956/medium_9781408236956.jpg', 1),
(36, 'Strange case of Dr Jekyll and Mr. Hyde', 'Stevenson, R-L', 'Pearson', 2013, '', 'Anglais', 'https://www.cooprosemont.qc.ca/ib/000023840000/CO_IMAGESBANK_MAST1/9781/4058/6245/5/9781405862455/medium_9781405862455.jpg', 1),
(37, 'Marrow Thieves (The)', 'Dimalinde, Cherie', 'Univeristy of Toronto Press', 2017, '', 'Anglais', 'https://www.cooprosemont.qc.ca/ib/000023840000/CO_IMAGESBANK_MAST1/9781/7708/6486/3/9781770864863/medium_9781770864863.jpg', 1);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

CREATE TABLE `utilisateurs` (
  `ID` int(13) NOT NULL,
  `PSEUDO` varchar(40) DEFAULT NULL,
  `MDP` varchar(40) DEFAULT NULL,
  `COURRIEL` varchar(40) DEFAULT NULL,
  `TELEPHONE` varchar(10) NOT NULL,
  `RANK` int(100) NOT NULL,
  `ROLE` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`ID`, `PSEUDO`, `MDP`, `COURRIEL`, `TELEPHONE`, `RANK`, `ROLE`) VALUES
(5, 'toto', 'toto', 'toto@gmail.com', '1234', 0, 'admin'),
(6, 'tata', 'tata', 'tata@gmail.com', '1234', 0, 'user'),
(8, 'mutian', 'mutian', 'mutian.wang@hotmail.com', '5142267510', 0, 'user'),
(9, 'daniel', 'daniel', 'daniel@hotmail.com', '5147572594', 100, 'admin');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `annonce`
--
ALTER TABLE `annonce`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `livres`
--
ALTER TABLE `livres`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `livres`
--
ALTER TABLE `livres`
  MODIFY `ID` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  MODIFY `ID` int(13) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
