INSTALLATION DU PROJET SUR NETBEANS

Le projet Booksell utilise les librairies de MySQL et de JTSL, donc il faut importer les fichiers .jar nécessaires
1.  Les 3 fichiers librairies .jar sont situées dans booksell/ressources/java-libraries du répertoire
2.  Ouvrir NetBeans 8.2
3.  Importer le projet
4.  Cliquer droit sur le projet Booksell situé dans le menu des projets (à gauche normalement)
5.  Cliquer sur "Propriété"
6.  Cliquer sur "Libraries"
7.  Supprimer tous les librairies déjà importées
8.  Cliquer "ADD JAR/FOLDER"
9.  Importer les trois fichiers dans le projet